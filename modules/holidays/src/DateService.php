<?php

namespace Drupal\holidays;

use DateTime;
/*use Drupal\Core\StringTranslation\StringTranslationTrait;*/
use Exception;

/**
  * Class DateService
  */
// Had to comment out StringTranslation and DateServiceInterface for phpunit to work!
/*class DateService extends DateTime implements DateServiceInterface {*/
class DateService extends DateTime {

/*  use StringTranslationTrait;*/

  /**
   * Testing for Drush command
   *
   * @return DateTime Date;
   */
  public function getCurrentTime(): DateTime
  {
    return new DateTime('2021-01-01');
  }

  /**
   * Checking whether it is a weekend
   *
   * @param DateTime $day
   * @return bool
   */
  public function isItAWeekend(DateTime $day): bool
  {
    $weekday = $day->format('w');
    if($weekday == 6 || $weekday == 0) {
      return true;
    }
    return false;
  }

  /**
   * Gets easter sunday go further calculate holidays
   * @param DateTime $year
   * @return DateTime
   * @throws Exception
   */
  public function getEasterSunday(DateTime $year): DateTime
  {
    $a = $year->format('Y') % 19;
    $b = (int)($year->format('Y') / 100);
    $c = (int)($year->format('Y') % 100);
    $d = (int)($b / 4);
    $e = $b % 4;
    $f = (int)(($b + 8) / 25);
    $g = (int)(($b - $f + 1) / 3);
    $h = (19 * $a + $b -$d -$g + 15) % 30;
    $i = (int)($c / 4);
    $k = $c % 4;
    $l = (32 + 2 * $e + 2 * $i - $h - $k) % 7;
    $m = (int)(($a + 11 * $h + 22 * $l) / 451);
    $month = (int)(($h + $l - 7 * $m +114) / 31);
    $day = ((($h + $l - 7 * $m + 114) % 31) + 1);
    return new DateTime($year->format('Y') . '-' . $month . '-' . $day);
  }

  /**
   * Gets first day of summer in Iceland for any given year
   * The first day of summer is always the third Thursday in April
   * @param DateTime $year
   * @return DateTime
   * @throws Exception
   */
  public function getFirstDayOfSummer(DateTime $year) : DateTime
  {
    $day = new DateTime($year->format('Y') . '-04-19');
    while(true) {
      if($day->format('w') == 4) {
        break;
      }
      $day = $day->modify('+1 day');
    }
    return $day;
  }

  /**
   * Gets the date for the Merchant holiday in Iceland for any given year
   * The Merchant holiday is always the first Monday in August
   * @param DateTime $year
   * @return DateTime
   * @throws Exception
   */
  public function getMerchantHoliday(DateTime $year) : DateTime
  {
    $day = new DateTime($year->format('Y') . '-08-01');
    while(true) {
      if($day->format('w') == 1) {
        break;
      }
      $day = $day->modify('+1 day');
    }
    return $day;
  }

  /**
   * Returns an array of all holidays for a given year
   * @param DateTime $year
   * @return array
   * @throws Exception
   */
  public function getAllHolidaysOverAYear(DateTime $year): array
  {
    $holidays = array();
    $holidays['new_years_day'] = new DateTime($year->format('Y') . '-01-01');
    $holidays['may_first'] = new DateTime($year->format('Y') . '-05-01');
    $holidays['june_seventeenth'] = new DateTime($year->format('Y') . '-06-17');
    $holidays['christmas_eve'] = new DateTime($year->format('Y'). '-12-24');
    $holidays['christmas_day'] = new DateTime($year->format('Y') . '-12-25');
    $holidays['second_of_xmas'] = new DateTime($year->format('Y') . '-12-26');
    $holidays['new_years_eve'] = new DateTime($year->format('Y') . '-12-31');
    $holidays['maundy_thursday'] = $this->getEasterSunday($year)->modify('-3 day');
    $holidays['great_friday'] = $this->getEasterSunday($year)->modify('-2 day');
    $holidays['easter_sunday'] = $this->getEasterSunday($year);
    $holidays['easter_monday'] = $this->getEasterSunday($year)->modify('+1 day');
    $holidays['ascension_of_jesus'] = $this->getEasterSunday($year)->modify('+39 day');
    $holidays['pentecost'] = $this->getEasterSunday($year)->modify('+49 day');
    $holidays['whit_monday'] = $this->getEasterSunday($year)->modify('+50 day');
    $holidays['first_day_of_summer'] = $this->getFirstDayOfSummer($year);
    $holidays['merchant_holiday'] = $this->getMerchantHoliday($year);

    return $holidays;
  }
}

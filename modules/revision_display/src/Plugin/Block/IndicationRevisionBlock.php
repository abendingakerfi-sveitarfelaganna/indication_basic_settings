<?php

namespace Drupal\revision_display\Plugin\Block;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResultReasonInterface;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

/**
 * Provides a block with revision information for indications
 *
 * @Block(
 *   id = "indication_revision_display",
 *   admin_label = @Translation("Indication Revision Display"),
 * )
 */
class IndicationRevisionBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $changed_data_header = [
      'what_was_changed' => t('What field was changed'),
      'when_was_it_changed' => t('When'),
      'old_value' => t('Old Value'),
      'new_value' => t('New Value'),
      'user_name_that_changed' => t('User who changed the value')
    ];
    $changed_data_array = $this->assembleRevisionData();

    $build['revision_table'] = [
      '#type' => 'table',
      '#header' => $changed_data_header,
      '#rows' => $changed_data_array,
      '#empty' => t('No Revision Data found')
    ];
    /**
     * @todo Create the table render array
     */
    return [
      '#type' => '#markup',
      '#markup' => \Drupal::service('renderer')->render($build),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account): AccessResultReasonInterface|AccessResult
  {
    return AccessResult::allowedIfHasPermission($account, 'view revisions block');
  }

  /**
   * {@inheritdoc}
   */
  #[Pure] public function blockForm($form, FormStateInterface $form_state): array {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['my_block_settings'] = $form_state->getValue('my_block_settings');
  }

  /**
   * Fetches all Revisions and returns an array of data, ready to be rendered by the block.
   *
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  private function assembleRevisionData() : array {
    $changed_data_array = [];

    $node = \Drupal::routeMatch()->getParameter('node');
    if($node) {
      $vids = \Drupal::entityTypeManager()->getStorage('node')->revisionIds($node);
      $revisions = null;
      foreach ($vids as $vid) {
        if ($revision = node_revision_load($vid)) {
          $revisions[] = $revision;
        }
      }

      $changed_data_array = $this->extractChangedData($revisions);
    }

    return $changed_data_array;
  }

  /**
   * Extracts the data from the query. Knows the data structure of an indication. If more fields are needed into the
   * render array, this function has to be altered.
   *
   * @param array $revision_data
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  private function extractChangedData(array $revision_data) : array {
    $changed_data = [];
    foreach($revision_data as $key => $revision_data_set) if($key == 0) {
      $revision_changer = $revision_data_set->getRevisionUser();
      if($revision_changer->id() == 0) {
        $revision_changer_name = t('Resident');
      }
      else {
        $full_name = strlen($revision_changer->get('field_full_name')->value) > 0
          ? $revision_changer->get('field_full_name')->value
          : 'Fullt nafn ekki skráð';
        $revision_changer_name = $full_name . " (" . $revision_changer->getAccountName() . ")";
      }

      $changed_data[] = $this->createChangedDataArray(
        t('Original submission'),
        date('j. F, Y H:i:s', $revision_data_set->getRevisionCreationTime()),
        '',
        '',
        $revision_changer_name
      );
    }
    else {
      $previous_revision = $revision_data[$key - 1];
      $revision_time_stamp = date('j. F, Y H:i:s', $revision_data_set->getRevisionCreationTime());
      $revision_changer = $revision_data_set->getRevisionUser();
      $full_name = strlen($revision_changer->get('field_full_name')->value) > 0
        ? $revision_changer->get('field_full_name')->value
        : 'Fullt nafn ekki skráð';

      $revision_changer_name = $full_name . " (" . $revision_changer->getAccountName() . ")";

      if(strcmp($prev_title = $previous_revision->getTitle(), $current_title = $revision_data_set->getTitle())) {
        $changed_data[] = $this->createChangedDataArray(t('Title'), $revision_time_stamp, $prev_title, $current_title, $revision_changer_name);
      }

      if($prev_delicate = $previous_revision->get('field_delicate_info')->value !=
        $current_delicate = $revision_data_set->get('field_delicate_info')->value) {
        $changed_data[] = $this->createChangedDataArray(
          t('Delicate information'),
          $revision_time_stamp,
          ($prev_delicate) ? 'TRUE' : 'FALSE',
          ($current_delicate) ? 'TRUE' : 'FALSE',
          $revision_changer_name
        );
      }

      if($prev_insurance = $previous_revision->get('field_indication_insurance_prob')->value !=
        $current_insurance = $revision_data_set->get('field_indication_insurance_prob')->value) {
        $changed_data[] = $this->createChangedDataArray(
          t('Insurance problem'),
          $revision_time_stamp,
          ($prev_insurance) ? 'TRUE' : 'FALSE',
          ($current_insurance) ? 'TRUE' : 'FALSE',
          $revision_changer_name
        );
      }

      if(strcmp($prev_email = $previous_revision->get('field_indication_reporter_email')->value,
        $current_email = $revision_data_set->get('field_indication_reporter_email')->value)) {
        $changed_data[] = $this->createChangedDataArray(
          t('Reporter email'),
          $revision_time_stamp,
          $prev_email,
          $current_email,
          $revision_changer_name
        );
      }

      if(strcmp($prev_reporter_name = $previous_revision->get('field_indication_reporter_name')->value,
        $current_reporter_name = $revision_data_set->get('field_indication_reporter_name')->value)) {
        $changed_data[] = $this->createChangedDataArray(
          t('Reporter name'),
          $revision_time_stamp,
          $prev_reporter_name,
          $current_reporter_name,
          $revision_changer_name
        );
      }

      if(strcmp($prev_reporter_phone = $previous_revision->get('field_indication_reporter_phone')->value,
        $current_reporter_phone = $revision_data_set->get('field_indication_reporter_phone')->value)) {
        $changed_data[] = $this->createChangedDataArray(
          t('Reporter phone number'),
          $revision_time_stamp,
          $prev_reporter_phone,
          $current_reporter_phone,
          $revision_changer_name
        );
      }

      if(strcmp($prev_indication_text_copy = $previous_revision->get('field_indication_text_copy')->value,
        $current_indication_text_copy = $revision_data_set->get('field_indication_text_copy')->value)) {
        $changed_data[] = $this->createChangedDataArray(
          t('Indication Text for Web'),
          $revision_time_stamp,
          $prev_indication_text_copy,
          $current_indication_text_copy,
          $revision_changer_name
        );
      }

      if(strcmp($prev_interpreted = trim(strip_tags($previous_revision->get('field_interpered_indication_text')->value)),
        $current_interpreted = trim(strip_tags($revision_data_set->get('field_interpered_indication_text')->value)))) {
        $changed_data[] = $this->createChangedDataArray(
          t('Interpreted Indication'),
          $revision_time_stamp,
          $prev_interpreted,
          $current_interpreted,
          $revision_changer_name
        );
      }

      // Type References
      $previous_indication_type_references = $this->extractTypeReferences($previous_revision->get('field_indication_type_ref')->getValue());
      $current_indication_type_references = $this->extractTypeReferences($revision_data_set->get('field_indication_type_ref')->getValue());
      $difference = array_diff($previous_indication_type_references, $current_indication_type_references);
      if(sizeof($difference) > 0) {
        $previous_indication_type_references = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadMultiple(
          $previous_indication_type_references
        );

        $current_indication_type_references = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadMultiple(
          $current_indication_type_references
        );
        $prev_string = "";
        $curr_string = "";

        foreach($previous_indication_type_references as $type_reference) {
          $prev_string .= $type_reference->name->value . " -> ";
        }
        $prev_string = substr($prev_string, 0, strlen($prev_string) -4);

        foreach($current_indication_type_references as $type_reference) {
          $curr_string .= $type_reference->name->value . " -> ";
        }
        $curr_string = substr($curr_string, 0,strlen($curr_string) - 4);

        $changed_data[] = $this->createChangedDataArray(
          t('Indication Type'),
          $revision_time_stamp,
          $prev_string,
          $curr_string,
          $revision_changer_name
        );
      }

      // Status References
      $previous_indication_status_ref = (int)$previous_revision->get('field_indication_status_ref')->getValue()[0]['target_id'];
      $current_indication_status_ref = (int)$revision_data_set->get('field_indication_status_ref')->getValue()[0]['target_id'];
      if($previous_indication_status_ref != $current_indication_status_ref) {
        $prev_string = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load(
          $previous_indication_status_ref
        )->name->value;
        $curr_string = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load(
          $current_indication_status_ref
        )->name->value;

        $changed_data[] = $this->createChangedDataArray(
          t('Indication Status'),
          $revision_time_stamp,
          $prev_string,
          $curr_string,
          $revision_changer_name
        );
      }

      $previous_location_data = $previous_revision->get('field_location_data')->value;
      $current_location_data = $revision_data_set->get('field_location_data')->value;
      if(is_array($previous_location_data) && is_array($current_location_data)) {
        $location_difference = array_diff($previous_location_data, $current_location_data);
        if(sizeof($location_difference) > 0) {
          $changed_data[] = $this->createChangedDataArray(
            t('Location Data'),
            $revision_time_stamp,
            $previous_location_data['value'],
            $current_location_data['value'],
            $revision_changer_name
          );
        }
      }

      $previous_internal_communications = $previous_revision->get('field_int_comm')->getValue();
      $current_internal_communications = $revision_data_set->get('field_int_comm')->getValue();
      $prev_string = "";
      $curr_string = "";
      foreach($previous_internal_communications as $internal_communication) {
        $prev_string .= $internal_communication['value'];
      }
      $prev_string = trim(strip_tags($prev_string));
      foreach($current_internal_communications as $internal_communication) {
        $curr_string .= $internal_communication['value'];
      }
      $curr_string = trim(strip_tags($curr_string));
      if(strcmp($prev_string, $curr_string)) {
        $changed_data[] = $this->createChangedDataArray(
          t('Internal Communications'),
          $revision_time_stamp,
          $prev_string,
          $curr_string,
          $revision_changer_name
        );
      }


      $previous_indication_response = $previous_revision->get('field_indication_response')->getValue();
      $previous_indication_response = ($previous_indication_response) ?
        $previous_indication_response[0]['value'] : "";
      $current_indication_response = $revision_data_set->get('field_indication_response')->getValue();
      $current_indication_response = ($current_indication_response) ?
        $current_indication_response[0]['value'] : "";
      if(strcmp($previous_indication_response, $current_indication_response)) {
        $changed_data[] = $this->createChangedDataArray(
          t('Indication Response'),
          $revision_time_stamp,
          ($previous_indication_response) ? trim(strip_tags($previous_indication_response)) : "",
          ($current_indication_response) ? trim(strip_tags($current_indication_response)) : "",
          $revision_changer_name
        );
      }

      $previous_neighbourhood_reference = null;
      $current_neighbourhood_reference = null;

      if($previous_revision->hasField('field_indication_neighborhoodref')) {
        $previous_neighbourhood_reference = $previous_revision->get('field_indication_neighborhoodref')->getValue();
      }
      elseif($previous_revision->hasField('field_indication_neighborhood')) {
        $previous_neighbourhood_reference = $previous_revision->get('field_indication_neighborhood')->getValue();
      }
      $previous_neighbourhood_reference = ($previous_neighbourhood_reference) ?
        (int)$previous_neighbourhood_reference[0]['target_id'] : null;

      if($revision_data_set->hasField('field_indication_neighborhoodref')) {
        $current_neighbourhood_reference = $revision_data_set->get('field_indication_neighborhoodref')->getValue();
      }
      elseif($revision_data_set->hasField('field_indication_neighborhood')) {
        $current_neighbourhood_reference = $revision_data_set->get('field_indication_neighborhood')->getValue();
      }

      $current_neighbourhood_reference = ($current_neighbourhood_reference) ?
        (int)$current_neighbourhood_reference[0]['target_id'] : null;

      if($previous_neighbourhood_reference != $current_neighbourhood_reference) {
        if(isset($previous_neighbourhood_reference)) {
          $prev_string = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load(
            $previous_neighbourhood_reference
          )->name->value;
        }
        else {
          $prev_string = "";
        }

        $curr_string = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load(
          $current_neighbourhood_reference
        )->name->value;

        $changed_data[] = $this->createChangedDataArray(
          t('Neighbourhood'),
          $revision_time_stamp,
          $prev_string,
          $curr_string,
          $revision_changer_name
        );
      }

      $previous_forward_to_external_dp = $previous_revision->get('field_forward_to_external_dp')->getValue();
      $previous_forward_to_external_dp = ($previous_forward_to_external_dp) ?
        (int)$previous_forward_to_external_dp[0]['target_id'] : null;
      $current_forward_to_external_dp = $revision_data_set->get('field_forward_to_external_dp')->getValue();
      $current_forward_to_external_dp = ($current_forward_to_external_dp) ?
        (int)$current_forward_to_external_dp[0]['target_id'] : null;

      if($previous_forward_to_external_dp != $current_forward_to_external_dp) {
        $prev_string = (!is_null($previous_forward_to_external_dp))
          ? \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load(
              $previous_forward_to_external_dp
            )->name->value
          : "";
        $curr_string = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load(
          $current_forward_to_external_dp
        )->name->value;

        $changed_data[] = $this->createChangedDataArray(
          t('Forward to External Department'),
          $revision_time_stamp,
          $prev_string,
          $curr_string,
          $revision_changer_name
        );
      }
      $a = 10;
    }

    return $changed_data;
  }

  #[ArrayShape(['what_was_changed' => "string", 'when_was_it_changed' => "int", 'old_value' => "string", 'new_value' => "string", 'user_name_that_changed' => "string"])]
  private function createChangedDataArray(string $what, string $when, string|null $old_value, string $new_value, string $who_changed) : array {
    $old_value = (is_null($old_value)) ? "" : $old_value;
    return [
      'what_was_changed' => $what,
      'when_was_it_changed' => $when,
      'old_value' => $old_value,
      'new_value' => $new_value,
      'user_name_that_changed' => $who_changed
    ];
  }

  private function extractTypeReferences(array $type_references) : array {
    foreach($type_references as $type_reference) {
      $return_arr[] = $type_reference['target_id'];
    }

    return $return_arr;
  }
}

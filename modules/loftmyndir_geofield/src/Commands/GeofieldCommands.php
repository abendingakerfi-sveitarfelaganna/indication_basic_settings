<?php

namespace Drupal\loftmyndir_geofield\Commands;

use Drupal\loftmyndir_geofield\LoftmyndirGeofieldConversionService;
use Drupal\node\Entity\Node;
use Drush\Commands\DrushCommands;

class GeofieldCommands extends DrushCommands {

  /**
   * Converts the old geofields to new
   *
   * @command loftmyndir-geofield-convert
   * @aliases lgc
   */
  public function convert() {
    /** @var LoftmyndirGeofieldConversionService $conversion_service */
    $conversion_service = \Drupal::service('loftmyndir_geofield.conversion_service');

    $counter = 0;
    $field_exists = $this->entityTypeHasField('field_loftmyndir_geofield');
    if(!$field_exists) {
      \Drupal::messenger()->addWarning(t("The bundle 'indications' does not have the field 'field_loftmyndir_geofield', therefor the operation could not continue."));
      return;
    }

    $indications = $this->fetchAllIndications();
    if(!$indications) {
      \Drupal::messenger()->addWarning(t('No data was found! Exiting'));
      return;
    }

    foreach($indications as $indication) {
      if($indication->get('field_loftmyndir_geofield')->isEmpty() && !$indication->get('field_location_data')->isEmpty()) {
        $location_data = $indication->get('field_location_data')->getValue()[0];

        $converted_points = $conversion_service->convertWGS84toISN93($location_data['lat'], $location_data['lng']);
        $location_points = $converted_points->toArray();
        if(($location_points[0] > 700000) || ($location_points[1] > 700000)) {
          continue;
        }
        $indication->set('field_loftmyndir_geofield', [
          'xcoord' => $location_points[0],
          'ycoord' => $location_points[1],
        ]);
        $indication->save();
        $counter++;
      }
      else {
        \Drupal::messenger()->addMessage('Geofield exists but Location data does not exists.');
      }
    }

    \Drupal::messenger()->addMessage(t('Done converting. Converted ' . $counter . ' indications out of ' . sizeof($indications)));
  }


  protected function fetchAllIndications() {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'indications')
      ->accessCheck(FALSE)
      ->execute();

    if((sizeof($query)) > 0) {
      return Node::loadMultiple($query);
    }
    else {
      return NULL;
    }
  }

  protected function entityTypeHasField($field_name) : bool {
    $all_bundle_fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', 'indications');
    return isset($all_bundle_fields[$field_name]);
  }
}

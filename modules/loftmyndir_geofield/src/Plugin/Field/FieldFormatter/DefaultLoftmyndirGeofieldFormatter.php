<?php

namespace Drupal\loftmyndir_geofield\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the 'default_loftmyndir_geofield_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "loftmyndir_geofield_cleartext_and_link_formatter",
 *   label = @Translation("Loftmyndir Geofield 'Cleartext and Link' Formatter"),
 *   field_types = {
 *     "loftmyndir_geofield"
 *   }
 * )
 */
class DefaultLoftmyndirGeofieldFormatter extends FormatterBase {

  use StringTranslationTrait;

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewValue($item);
    }

    return $elements;
  }

  protected function viewValue(FieldItemInterface $item) : array {
    $xcoord = $item->get('xcoord')->getValue();
    $ycoord = $item->get('ycoord')->getValue();
    return [
      '#theme' => 'loftmyndir_geofield',
      '#xcoord' => $xcoord,
      '#ycoord' => $ycoord
    ];
  }
}

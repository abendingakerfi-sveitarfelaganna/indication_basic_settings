<?php

namespace Drupal\loftmyndir_geofield\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the 'loftmyndir_geofield_cleartext_and_link_widget' widget.
 *
 * @FieldWidget(
 *    id = "loftmyndir_geofield_cleartext_and_link_widget",
 *    label = @Translation("Loftmyndir Geofield Cleartext Widget"),
 *    field_types = {
 *      "loftmyndir_geofield"
 *    }
 * )
 */
class DefaultLoftmyndirGeofieldWidget extends WidgetBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'placeholder' => [
        'xcoord' => 0,
        'ycoord' => 0
      ],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) : array {
    $elements = [];

    $elements['placeholder'] = [
      '#type' => 'details',
      '#title' => $this->t('Placeholder'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    $placeholder_settings = $this->getSetting('placeholder');
    $elements['placeholder']['xcoord'] = [
      '#type' => 'textfield',
      '#title' => $this->t('X-Coordination field'),
      '#default_value' => $placeholder_settings['xcoord'],
    ];
    $elements['placeholder']['ycoord'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Y-Coordination field'),
      '#default_value' => $placeholder_settings['ycoord'],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $placeholder_settings = $this->getSetting('placeholder');
    if (!empty($placeholder_settings['xcoord']) && !empty ($placeholder_settings['ycoord'])) {
      $summary[] = $this->t('X-Coordination placeholder: @xcoord', ['@xcoord' => $placeholder_settings['xcoord']]);
      $summary[] = $this->t('Y-Coordination placeholder: @ycoord', ['@ycoord' => $placeholder_settings['ycoord']]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['details'] = [
      '#type' => 'details',
      '#title' => $element['#title'],
      '#open' => $this->getSetting('fieldset_state') == 'open' ? TRUE : FALSE,
      '#description' => $element['#description'],
    ] + $element;

    $placeholder_settings = $this->getSetting('placeholder');
    $element['details']['xcoord'] = [
      '#type' => 'textfield',
      '#title' => $this->t('X-Coordination'),
      '#default_value' => isset($items[$delta]->xcoord) ? $items[$delta]->xcoord : NULL,
      '#size' => $this->getSetting('xcoord_size'),
      '#placeholder' => $placeholder_settings['xcoord'],
      '#description' => $this->t('The X-Coordination of the point being saved'),
      '#required' => $element['#required'],
    ];

    $element['details']['ycoord'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Y-Coordination'),
      '#default_value' => isset($items[$delta]->ycoord) ? $items[$delta]->ycoord : NULL,
      '#size' => $this->getSetting('ycoord_size'),
      '#placeholder' => $placeholder_settings['ycoord'],
      '#description' => $this->t('The Y-Coordination of the point being saved'),
      '#required' => $element['#required'],
    ];

    $link = (isset($items[$delta]->xcoord) && (isset($items[$delta]->ycoord)))
      ? "<a href='https://map.is/hafnarfjordur_innri/?action=pin{lon:" . $items[$delta]->xcoord . ",lat:" . $items[$delta]->ycoord . ",zoom:10}'>Skoða hnit á map.is</a>"
      : "<span class='no-geolocation-coordinates'>Engin hnit skráð til að birta hlekk fyrir map.is</span>";
    $element['loftmyndir_link'] = [
      '#type' => '#markup',
      '#markup' => $link
    ];
    // https://map.is/hafnarfjordur_innri/?action=pin{lon:{{ xcoord }},lat:{{ ycoord }},zoom:10}
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) : array {
    foreach ($values as &$value) {
      $value['xcoord'] = $value['details']['xcoord'];
      $value['ycoord'] = $value['details']['ycoord'];
      unset($value['details']);
    }

    return $values;
  }
}

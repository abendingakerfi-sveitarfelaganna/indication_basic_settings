<?php

namespace Drupal\loftmyndir_geofield\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of 'loftmyndir_geofield' field type
 *
 * @FieldType(
 *   id = "loftmyndir_geofield",
 *   label = @Translation("Loftmyndir Geofield"),
 *   description = @Translation("Field for storing Geographical data in the format Loftmyndir uses"),
 *   default_widget = "loftmyndir_geofield_cleartext_and_link_widget",
 *   default_formatter = "loftmyndir_geofield_cleartext_and_link_formatter"
 * )
 */
class MapFieldItem extends FieldItemBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() : array {
    return [
      'xcoord_max_length' => 255,
      'ycoord_max_length' => 255,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {
    $elements = [];

    $elements['xcoord_max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('X-Coordination maximum length'),
      '#default_value' => $this->getSetting('xcoord_max_length'),
      '#required' => TRUE,
      '#description' => $this->t('Maximum length for the X-Coordination in characters'),
      '#min' => 1,
      '#disabled' => $has_data,
    ];

    $elements['ycoord_max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Y-Coordination maximum length'),
      '#default_value' => $this->getSetting('ycoord_max_length'),
      '#required' => TRUE,
      '#description' => $this->t('Maximum length for the Y-Coordination in characters'),
      '#min' => 1,
      '#disabled' => $has_data,
    ];

    return $elements + parent::storageSettingsForm($form, $form_state, $has_data);
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'xcoord' => [
          'type' => 'numeric',
          'precision' => 18,
          'scale' => 12,
          'not null' => FALSE,
        ],
        'ycoord' => [
          'type' => 'numeric',
          'precision' => 18,
          'scale' => 12,
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) : array {
    $properties['xcoord'] = DataDefinition::create('float')
      ->setLabel(t('X-Coordinates (Centroid Latitude)'));

    $properties['ycoord'] = DataDefinition::create('float')
      ->setLabel(t('Y-Coordinates (Centroid Longitude)'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) : array {
    $values['xcoord'] = mt_rand(353000, 374000);
    $values['ycoord'] = mt_rand(396000, 413000);

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $xcoord = $this->get('xcoord')->getValue();
    $ycoord = $this->get('ycoord')->getValue();
    return $xcoord === NULL || $xcoord === '' || $ycoord === NULL || $ycoord === '';
  }
}

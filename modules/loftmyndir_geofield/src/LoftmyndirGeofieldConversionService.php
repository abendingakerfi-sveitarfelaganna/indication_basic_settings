<?php

namespace Drupal\loftmyndir_geofield;

use Drupal\Core\Config\ConfigFactoryInterface;
use proj4php\Point;
use proj4php\Proj;
use proj4php\Proj4php;

class LoftmyndirGeofieldConversionService {
  /**
   * @var ConfigFactoryInterface $configFactory
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * @var \proj4php\Proj4php
   */
  protected Proj4php $proj4;

  /**
   * @var \proj4php\Proj
   */
  protected Proj $projWGS84;

  /**
   * @var \proj4php\Proj
   */
  protected Proj $proj3057;

  /**
   *  LoftmyndirGeofieldConversionService constructor.
   *
   * @param ConfigFactoryInterface $config_factory
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;

    $this->proj4 = new Proj4php();
    $this->projWGS84 = new Proj('EPSG:4326', $this->proj4);
    $this->proj3057 = new Proj('EPSG:3057', $this->proj4);
  }

  /**
   * Converts WGS84 to ISN93.
   *
   * In other words, converts from Google co-ordinates (lat/long) to co-ordinates used at Landmælingar Íslands.
   * @param float $lat
   * @param float $long
   *
   * @return false|mixed|\proj4php\Point
   */
  public function convertWGS84toISN93(float $lat, float $long) : Point {
    $pointSrc = new Point($long, $lat, $this->projWGS84);
    return $this->proj4->transform($this->proj3057, $pointSrc);
  }
}

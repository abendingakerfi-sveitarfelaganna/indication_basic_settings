<?php
namespace Drupal\isitaworkday\Commands;
use Drupal;
use Drupal\isitaworkday\CheckDateService;
use Drush\Commands\DrushCommands;
use Exception;

/**
 * A Drush command file.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 * - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 * - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class IsItAWorkdayCommands extends DrushCommands {
  /**
   * @command isitaworkday:test_isitaworkday
   * @aliases isitaworkday
   * @usage isitaworkday:test_isitaworkday
   *   We are testing if it is a workday!
   * @throws Exception
   */
  public function testIsItAWorkday() {
    /** @var CheckDateService $service */
    $service = Drupal::service('isitaworkday.date_service');
    $is_it_a_workday = $service->isItAWorkday(new Drupal\holidays\DateService('2021-04-04'));
    echo $is_it_a_workday;
    $a = 10;
  }

  /**
   * @command isitaworkday:test_getlastresponsedate
   * @aliases getlastresponsedate
   * @usage isitaworkday:test_getlastresponsedate
   *   We are testing last response date!
   * @throws Exception
   */
  public function testGetLastResponseDate() {
    /** @var CheckDateService $service */
    $service = Drupal::service('isitaworkday.date_service');
    $get_last_response_date = $service->getLastResponseDate(new Drupal\holidays\DateService('2021-03-21'. '11:20'), '2')->format('Y-M-d-H:i');
    echo $get_last_response_date;
    $a = 10;
  }
}

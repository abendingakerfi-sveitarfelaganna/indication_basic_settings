<?php
namespace Drupal\indication\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class IndicationHasBeenProcessedConfigurationForm extends ConfigFormBase
{
  protected function getEditableConfigNames() {
    return ['indication.indication_has_been_processed_configuration'];
  }

  public function getFormId() {
    return 'indication_has_been_processed_configuration_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('indication.indication_has_been_processed_configuration');
    /**$form['autoclose_when_answered'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Close indication when it is answered'),
      '#description' => $this->t('Determines if the indication should be automatically closed when it has been answered. <b> If it is closed by answering then it will not send the default closing email!</b>'),
      '#default_value' => $config->get('autoclose_when_answered'),
    ];*/

    $form['send_email_when_closed'] = [
      '#type' => 'checkbox',
      '#title' => t('Send an email when indication is closed'),
      '#description' => t('Determines if an email is sent to the one who reported, when the indication is closed.'),
      '#default_value' => ($config->get('send_email_when_closed')) ? $config->get('send_email_when_closed') : FALSE,
    ];

    $form['indication_has_been_processed_subject'] = [
      '#type' => 'textfield',
      '#title' => t('Indication has been processed - subject line'),
      '#description' => t('The subject line for the email sent to a reporter after the indication has been processed.<br /><b>Available tokens:</b> [ID], [assigned_to], [reporter_name]'),
      '#default_value' => ($config->get('indication_has_been_processed_subject')) ? $config->get('indication_has_been_processed_subject') : ""
    ];

    $form['email'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Email body, when auto-notifying of indication closed'),
      '#description' => $this->t('Configuration for the email sent to the reporter when indication is closed if there was no answer.'),
      '#states' => [
        'visible' => [
          ':input[name="send_email_when_closed"]' => ['checked' => true],
        ],
      ],
    ];



    $form['email']['indication_has_been_processed_body'] = [
      '#type' => 'text_format',
      '#title' => t('Indication has been processed - body'),
      '#format' => $config->get('indication_has_been_processed_format') ? $config->get('indication_has_been_processed_format') : "rich_text",
      '#description' => t('The subject line for the email sent to a reporter after the indication has been processed. <br /><b>Available tokens:</b> [ID], [reporter_name],[indication]'),
      '#default_value' => $config->get('indication_has_been_processed_body') ? $config->get('indication_has_been_processed_body') : ""
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->config('indication.indication_has_been_processed_configuration')
      ->set('autoclose_when_answered', $form_state->getValue('autoclose_when_answered'))
      ->save();

    $this->config('indication.indication_has_been_processed_configuration')
      ->set('send_email_when_closed', $form_state->getValue('send_email_when_closed'))
      ->save();

    $this->config('indication.indication_has_been_processed_configuration')
      ->set('indication_has_been_processed_subject', $form_state->getValue('indication_has_been_processed_subject'))
      ->save();

    $this->config('indication.indication_has_been_processed_configuration')
      ->set('indication_has_been_processed_body', $form_state->getValue('indication_has_been_processed_body')['value'])
      ->save();

    $this->config('indication.indication_has_been_processed_configuration')
      ->set('indication_has_been_processed_format', $form_state->getValue('indication_has_been_processed_body')['format'])
      ->save();


    parent::submitForm($form, $form_state);
  }
}

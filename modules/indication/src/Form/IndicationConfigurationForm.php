<?php
namespace Drupal\indication\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class IndicationConfigurationForm extends ConfigFormBase
{
  protected function getEditableConfigNames() {
    return ['indication.indication_configuration'];
  }

  public function getFormId() {
    return 'indication_configuration_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('indication.indication_configuration');

    $form['publish_indications'] = [
      '#type' => 'checkbox',
      '#title' => t('Publish indications after they have been approved'),
      '#description' => t('Determines if an indication should be automatically published after they have been assigned (and therefore approved). NOTE: If turned on, this will ONLY affect the indications that are created after the fact!'),
      '#default_value' => ($config->get('publish_indications')) ? $config->get('publish_indications') : FALSE,
    ];


    $form['general_user_name'] = [
      '#type' => 'textfield',
      '#title' => t('General user name.'),
      '#description' => t('When the email is sent out, it requires a generic "user" name when the user did not enter his/hers name in the name field'),
      '#default_value' => ($config->get('general_user_name')) ? $config->get('general_user_name') : ""
    ];

    $form['general_signature'] = [
      '#type' => 'text_format',
      '#title' => t('General email signature'),
      '#description' => t('Used when no signature is provided by user or department.'),
      '#default_value' => ($config->get('general_signature')) ?? '',
    ];

    $form['service_mode'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Service mode'),
      '#description' => $this->t('Configuration for the service mode the module can be set in.'),
      '#states' => [
        'visible' => [
          ':input[name="service-mode-enabled"]' => ['checked' => true],
        ],
      ],
    ];

    $form['service_mode']['service_mode_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Service Mode'),
      '#description' => $this->t('Service mode disables the pre-save function for Indications. '),
      '#default_value' => (bool) $config->get('service_mode_enabled')
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('indication.indication_configuration')
      ->set('publish_indications', $form_state->getValue('publish_indications'))
      ->save();

    $this->config('indication.indication_configuration')
      ->set('general_user_name', $form_state->getValue('general_user_name'))
      ->save();

    $this->config('indication.indication_configuration')
      ->set('general_signature', $form_state->getValue('general_signature')['value'])
      ->save();

    $this->config('indication.indication_configuration')
      ->set('general_signature_format', $form_state->getValue('general_signature')['format'])
      ->save();

    $this->config('indication.indication_configuration')
      ->set('service_mode_enabled', $form_state->getValue('service_mode_enabled'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}

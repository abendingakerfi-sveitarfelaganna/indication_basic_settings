<?php
namespace Drupal\indication\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ThanksForSubmittingConfigurationForm extends ConfigFormBase
{
  protected function getEditableConfigNames() {
    return ['indication.thanks_for_submitting_configuration'];
  }

  public function getFormId() {
    return 'thanks_for_submitting_configuration_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('indication.thanks_for_submitting_configuration');


    $form['thank_you_for_submitting_redirect_url'] = [
      '#type' => 'textfield',
      '#title' => t('Thank you for submitting - redirect url'),
      '#description' => t('The URL that the user will be redirected to, after submitting the indication. It HAS TO start with a /'),
      '#default_value' => ($config->get('thank_you_for_submitting_redirect_url')) ? $config->get('thank_you_for_submitting_redirect_url') : "/"
    ];

    $form['send_email_when_created'] = [
      '#type' => 'checkbox',
      '#title' => t('Send email when indication is created'),
      '#description' => t('Determines if an email is sent to the reporter an indication by default or not.'),
      '#default_value' => $config->get('send_email_when_created')
    ];

    $form['email'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Email to reporter'),
      '#description' => $this->t('Configuration for the email sent to assigned employee and department.'),
      '#states' => [
        'visible' => [
          ':input[name="send_email_when_created"]' => ['checked' => true],
        ]
      ]
    ];

    $form['email']['thank_you_for_submitting_subject'] = [
      '#type' => 'textfield',
      '#title' => t('Thank you for submitting - subject line'),
      '#description' => t('The subject line for the email sent to a reporter.<br /><b>Available tokens:</b> [ID], [reporter_name],[indication]'),
      '#default_value' => ($config->get('thank_you_for_submitting_subject')) ? $config->get('thank_you_for_submitting_subject') : ""
    ];

    $form['email']['thank_you_for_submitting_body'] = [
      '#type' => 'text_format',
      '#title' => t('Thank you for submitting - body'),
      '#format' => $config->get('thank_you_for_submitting_format') ? $config->get('thank_you_for_submitting_format') : "rich_text",
      '#description' => t('The email message sent to a reporter after they submit a new indication.<br /><b>Available tokens:</b> [ID], [reporter_name],[indication]'),
      '#default_value' => $config->get('thank_you_for_submitting_body') ? $config->get('thank_you_for_submitting_body') : ""
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->config('indication.thanks_for_submitting_configuration')
      ->set('send_email_when_created', $form_state->getValue('send_email_when_created'))
      ->save();

    $this->config('indication.thanks_for_submitting_configuration')
      ->set('thank_you_for_submitting_redirect_url', $form_state->getValue('thank_you_for_submitting_redirect_url'))
      ->save();

    $this->config('indication.thanks_for_submitting_configuration')
      ->set('thank_you_for_submitting_subject', $form_state->getValue('thank_you_for_submitting_subject'))
      ->save();

    $this->config('indication.thanks_for_submitting_configuration')
      ->set('thank_you_for_submitting_body', $form_state->getValue('thank_you_for_submitting_body')['value'])
      ->save();

    $this->config('indication.thanks_for_submitting_configuration')
      ->set('thank_you_for_submitting_format', $form_state->getValue('thank_you_for_submitting_body')['format'])
      ->save();

    parent::submitForm($form, $form_state);
  }
}

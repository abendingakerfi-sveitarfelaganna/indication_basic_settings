<?php
namespace Drupal\indication\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class IndicationHasBeenAssignedConfigurationForm extends ConfigFormBase
{
  protected function getEditableConfigNames() {
    return ['indication.indication_has_been_assigned_configuration'];
  }

  public function getFormId() {
    return 'indication_has_been_assigned_configuration_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('indication.indication_has_been_assigned_configuration');

    $form['send_email_when_assigned'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send an email to reporter when indication is assigned to an internal department'),
      '#description' => $this->t('Determines if an email is sent to the one who reported, when the indication is assigned internally.'),
      '#default_value' => $config->get('send_email_when_assigned'),
    ];

    $form['email_reporter'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Email to reporter'),
      '#description' => $this->t('Configuration for the email sent to assigned employee and department.'),
      '#states' => [
        'visible' => [
          ':input[name="send_email_when_assigned"]' => ['checked' => true]
        ],
      ],
    ];

    $form['email_reporter']['indication_has_been_assigned_subject_reporter'] = [
      '#type' => 'textfield',
      '#title' => t('Indication has been assigned - subject line'),
      '#description' => t('The subject line for the email sent to a department after the indication has been assigned. <br /><b>Available tokens:</b> [ID], [assigned_to], [reporter_name]'),
      '#default_value' => ($config->get('indication_has_been_assigned_subject_reporter')) ? $config->get('indication_has_been_assigned_subject_reporter') : ""
    ];

    $form['email_reporter']['indication_has_been_assigned_body_reporter'] = [
      '#type' => 'text_format',
      '#title' => t('Indication has been assigned - body'),
      '#format' => $config->get('indication_has_been_assigned_reporter_format') ? $config->get('indication_has_been_assigned_reporter_format') : "rich_text",
      '#description' => t('The email message sent to a department after the indication has been assigned.<br /><b>Available tokens:</b> [ID], [assigned_to], [reporter_name]'),
      '#default_value' => $config->get('indication_has_been_assigned_body_reporter') ? $config->get('indication_has_been_assigned_body_reporter') : ""
    ];

    $form['email'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Email to assignee'),
      '#description' => $this->t('Configuration for the email sent to assigned employee and department.'),
    ];

    $form['email']['indication_has_been_assigned_subject'] = [
      '#type' => 'textfield',
      '#title' => t('Indication has been assigned - subject line'),
      '#description' => t('The subject line for the email sent to a department after the indication has been assigned.<br /><b>Available tokens:</b> [ID], [assigned_to], [reporter_name]'),
      '#default_value' => ($config->get('indication_has_been_assigned_subject')) ? $config->get('indication_has_been_assigned_subject') : ""
    ];

    $form['email']['indication_has_been_assigned_body'] = [
      '#type' => 'text_format',
      '#title' => t('Indication has been assigned - body'),
      '#format' => $config->get('indication_has_been_assigned_format') ? $config->get('indication_has_been_assigned_format') : "rich_text",
      '#description' => t('The email message sent to a department after the indication has been assigned.<br /><b>Available tokens:</b> [ID], [assigned_to], [reporter_name], [indication]'),
      '#default_value' => $config->get('indication_has_been_assigned_body') ? $config->get('indication_has_been_assigned_body') : ""
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->config('indication.indication_has_been_assigned_configuration')
      ->set('send_email_when_assigned', $form_state->getValue('send_email_when_assigned'))
      ->save();

    $this->config('indication.indication_has_been_assigned_configuration')
      ->set('indication_has_been_assigned_subject_reporter', $form_state->getValue('indication_has_been_assigned_subject_reporter'))
      ->save();

    $this->config('indication.indication_has_been_assigned_configuration')
      ->set('indication_has_been_assigned_body_reporter', $form_state->getValue('indication_has_been_assigned_body_reporter')['value'])
      ->save();

    $this->config('indication.indication_has_been_assigned_configuration')
      ->set('indication_has_been_assigned_reporter_format', $form_state->getValue('indication_has_been_assigned_body_reporter')['format'])
      ->save();

    $this->config('indication.indication_has_been_assigned_configuration')
      ->set('indication_has_been_assigned_subject', $form_state->getValue('indication_has_been_assigned_subject'))
      ->save();

    $this->config('indication.indication_has_been_assigned_configuration')
      ->set('indication_has_been_assigned_body', $form_state->getValue('indication_has_been_assigned_body')['value'])
      ->save();

    $this->config('indication.indication_has_been_assigned_configuration')
      ->set('indication_has_been_assigned_format', $form_state->getValue('indication_has_been_assigned_body')['format'])
      ->save();


    parent::submitForm($form, $form_state);
  }
}

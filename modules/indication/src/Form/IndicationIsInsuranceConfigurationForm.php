<?php
namespace Drupal\indication\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class IndicationIsInsuranceConfigurationForm extends ConfigFormBase {

  protected function getEditableConfigNames() : array {
    return ['indication.indication_is_insurance_configuration'];
  }

  public function getFormId() {
    return 'indication_is_insurance_configuration_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('indication.indication_is_insurance_configuration');

    $form['autoclose_when_insurance'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Close indication when it is insurance matter'),
      '#description' => $this->t('Determines if an indication should be closed if it is insurance matter.'),
      '#default_value' => $config->get('autoclose_when_insurance'),
    ];

    $form['indication_is_insurance_lawyers_salutation'] = [
      '#type' => 'textfield',
      '#title' => t('Lawyers\' salutation'),
      '#description' => t('If it is a person, enter their name, if it is a department, enter the departments name. (This is not always used!)'),
      '#default_value' => ($config->get('indication_is_insurance_lawyers_salutation')) ? $config->get('indication_is_insurance_lawyers_salutation') : ""
    ];

    $form['indication_is_insurance_lawyers_email'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('Lawyers\' email'),
      '#description' => t('This is the email (or postbox group) for the lawyer(s) handling all insurance matters.'),
      '#default_value' => ($config->get('indication_is_insurance_lawyers_email')) ? $config->get('indication_is_insurance_lawyers_email') : ""
    ];

    $form['indication_is_insurance_lawyers_subject'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('Lawyers\' subject line'),
      '#description' => t('The subject line for the email sent to the lawyer(s) after the indication has been categorized as insurance matter.<br /><b>Available tokens:</b> [ID], [reporter_name]'),
      '#default_value' => ($config->get('indication_is_insurance_lawyers_subject')) ? $config->get('indication_is_insurance_lawyers_subject') : ""
    ];

    $form['indication_is_insurance_lawyers_body'] = [
      '#type' => 'text_format',
      '#required' => TRUE,
      '#title' => t('Lawyers\' body'),
      '#format' => $config->get('indication_is_insurance_lawyers_format') ? $config->get('indication_is_insurance_lawyers_format') : "rich_text",
      '#description' => t('The email message sent to the lawyer(s) after the indication has been categorized as insurance matter.<br /><b>Available tokens:</b> [ID], [reporter_name],[indication]'),
      '#default_value' => $config->get('indication_is_insurance_lawyers_body') ? $config->get('indication_is_insurance_lawyers_body') : ""
    ];

    $form['send_email_when_insurance'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send an email to reporter when indication is insurance matter'),
      '#description' => $this->t('Determines if an email is sent to the one who reported, when the indication is a insurance matter. If you choose to close the indication when it is insurance matter then this will be sent instead of the auto close reply'),
      '#default_value' => $config->get('send_email_when_insurance'),
    ];

    $form['email_reporter'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Email to reporter'),
      '#description' => $this->t('Configuration for the email sent to assigned employee and department.'),
      '#states' => [
        'visible' => [
          ':input[name="send_email_when_insurance"]' => ['checked' => true]
        ],
      ],
    ];

    $form['email_reporter']['indication_is_insurance_subject'] = [
      '#type' => 'textfield',
      '#title' => t('Reporters\'s subject line'),
      '#description' => t('The subject line for the email sent to the reporter after the indication has been categorized as insurance matter.<br /><b>Available tokens:</b> [ID], [assigned_to], [reporter_name]'),
      '#default_value' => ($config->get('indication_is_insurance_subject')) ? $config->get('indication_is_insurance_subject') : ""
    ];

    $form['email_reporter']['indication_is_insurance_body'] = [
      '#type' => 'text_format',
      '#title' => t('Reporters\' email body'),
      '#format' => $config->get('indication_is_insurance_format') ? $config->get('indication_is_insurance_format') : "rich_text",
      '#description' => t('The email message sent to the reporter after the indication has been categorized as insurance matter.<br /><b>Available tokens:</b> [ID], [assigned_to], [reporter_name], [indication]'),
      '#default_value' => $config->get('indication_is_insurance_body') ? $config->get('indication_is_insurance_body') : ""
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('indication.indication_is_insurance_configuration')
      ->set('autoclose_when_insurance', $form_state->getValue('autoclose_when_insurance'))
      ->save();

    $this->config('indication.indication_is_insurance_configuration')
      ->set('indication_is_insurance_lawyers_salutation', $form_state->getValue('indication_is_insurance_lawyers_salutation'))
      ->save();

    $this->config('indication.indication_is_insurance_configuration')
      ->set('indication_is_insurance_lawyers_email', $form_state->getValue('indication_is_insurance_lawyers_email'))
      ->save();

    $this->config('indication.indication_is_insurance_configuration')
      ->set('indication_is_insurance_lawyers_subject', $form_state->getValue('indication_is_insurance_lawyers_subject'))
      ->save();

    $this->config('indication.indication_is_insurance_configuration')
      ->set('indication_is_insurance_lawyers_body', $form_state->getValue('indication_is_insurance_lawyers_body')['value'])
      ->save();

    $this->config('indication.indication_is_insurance_configuration')
      ->set('indication_is_insurance_lawyers_format', $form_state->getValue('indication_is_insurance_lawyers_body')['format'])
      ->save();

    $this->config('indication.indication_is_insurance_configuration')
      ->set('indication_is_insurance_subject', $form_state->getValue('indication_is_insurance_subject'))
      ->save();

    $this->config('indication.indication_is_insurance_configuration')
      ->set('send_email_when_insurance', $form_state->getValue('send_email_when_insurance'))
      ->save();

    $this->config('indication.indication_is_insurance_configuration')
      ->set('indication_is_insurance_body', $form_state->getValue('indication_is_insurance_body')['value'])
      ->save();

    $this->config('indication.indication_is_insurance_configuration')
      ->set('indication_is_insurance_format', $form_state->getValue('indication_is_insurance_body')['format'])
      ->save();

    parent::submitForm($form, $form_state);
  }
}

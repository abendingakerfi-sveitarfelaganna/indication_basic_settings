<?php
namespace Drupal\indication\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class IndicationHasPassedDueDateForm extends ConfigFormBase
{
  protected function getEditableConfigNames() {
    return ['indication.indication_has_passed_due_date_configuration'];
  }

  public function getFormId() {
    return 'indication_has_passed_due_date_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('indication.indication_has_passed_due_date_configuration');
    $form['send_email_when_indication_has_passed_due_date'] = [
      '#type' => 'checkbox',
      '#title' => t('Send an email when indication has passed it\'s due date.'),
      '#description' => t('Determines if an email is sent to the one assigned to the indication, when it\'s passed it\'s due date.'),
      '#default_value' => ($config->get('send_email_when_indication_has_passed_due_date')) ? $config->get('send_email_when_indication_has_passed_due_date') : FALSE,
    ];

    $form['email'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Email body, when auto-notifying of indication is passed due date.'),
      '#description' => $this->t('Configuration for the email sent to the assignee when indication is passed due date.'),
      '#states' => [
        'visible' => [
          ':input[name="send_email_when_indication_has_passed_due_date"]' => ['checked' => true],
        ],
      ],
    ];

    $form['email']['indication_has_passed_due_date_subject'] = [
      '#type' => 'textfield',
      '#title' => t('Indication has passed it\'s due date - subject line'),
      '#description' => t('The subject line for the email sent to the one assigned to the indication.<br /><b>Available tokens:</b> [ID], [assigned_to]'),
      '#default_value' => ($config->get('indication_has_passed_due_date_subject')) ? $config->get('indication_has_passed_due_date_subject') : ""
    ];

    $form['email']['indication_has_passed_due_date_body'] = [
      '#type' => 'text_format',
      '#title' => t('Indication has passed it\'s due date - body'),
      '#format' => $config->get('indication_has_passed_due_date_format') ? $config->get('indication_has_passed_due_date_format') : "rich_text",
      '#description' => t('The body for the email sent to the one assigned to the indication. <br /><b>Available tokens:</b> [ID], [indication], [assigned_date]'),
      '#default_value' => $config->get('indication_has_passed_due_date_body') ? $config->get('indication_has_passed_due_date_body') : ""
    ];

    $form['email']['indication_has_passed_due_date_status'] = [
      '#type' => 'select',
      '#title' => t('Select which status is effected by this email'),
      '#options' => [
        'received' => t('Received'),
        'both' => t('Both Received and In Progress'),
      ],
      '#default_value' => $config->get('indication_has_passed_due_date_status') ? $config->get('indication_has_passed_due_date_status') : "received",
    ];

    $form['email']['indication_has_passed_due_date_leeway'] = [
      '#type' => 'select',
      '#title' => t('Select how much leeway after due date, the assignee has.'),
      '#options' => [
        0 => 'No leeway',
        1 => '1 day',
        2 => '2 days',
        3 => '3 days',
        4 => '4 days',
        5 => '5 days',
        6 => '6 days',
        7 => '7 days'
      ],
      '#default_value' => $config->get('indication_has_passed_due_date_leeway') ? $config->get('indication_has_passed_due_date_leeway') : 0,
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('indication.indication_has_passed_due_date_configuration');
    $config->set('send_email_when_indication_has_passed_due_date', $form_state->getValue('send_email_when_indication_has_passed_due_date'))
      ->save();

    $config->set('indication_has_passed_due_date_subject', $form_state->getValue('indication_has_passed_due_date_subject'))
      ->save();

    $config->set('indication_has_passed_due_date_body', $form_state->getValue('indication_has_passed_due_date_body')['value'])
      ->save();

    $config->set('indication_has_passed_due_date_status', $form_state->getValue('indication_has_passed_due_date_status'))
      ->save();

    $config->set('indication_has_passed_due_date_leeway', $form_state->getValue('indication_has_passed_due_date_leeway'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}

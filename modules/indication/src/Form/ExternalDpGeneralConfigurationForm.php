<?php
namespace Drupal\indication\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ExternalDpGeneralConfigurationForm extends ConfigFormBase
{
  protected function getEditableConfigNames() {
    return ['indication.external_dp_general_configuration'];
  }

  public function getFormId() {
    return 'external_dp_general_configuration_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('indication.external_dp_general_configuration');


    $form['send_email_when_forwarded'] = [
      '#type' => 'checkbox',
      '#title' => t('Send an email when indication is forwarded to an external department'),
      '#description' => t('Determines if an email is sent to the one who reported, when the indication is forwarded to an external department like Vegagerðin or Lögreglan.'),
      '#default_value' => ($config->get('send_email_when_forwarded')) ? $config->get('send_email_when_forwarded') : FALSE,
    ];

    $form['email'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Email'),
      '#description' => $this->t('Configuration for the email sent to assigned employee and department.'),
      '#states' => [
        'visible' => [
          ':input[name="send_email_when_forwarded"]' => ['checked' => true]
        ],
      ],
    ];

    $form['email']['external_dp_general_subject'] = [
      '#type' => 'textfield',
      '#title' => t('Indication has been forwarded to you - general subject line'),
      '#description' => t('The subject line for all emails sent to an external department. <br /><b>Available tokens:</b> [ID], [assigned_to], [reporter_name]'),
      '#default_value' => ($config->get('external_dp_general_subject')) ? $config->get('external_dp_general_subject') : ""
    ];

    $form['email']['external_dp_general_body'] = [
      '#type' => 'text_format',
      '#title' => t('Indication has been forwarded to you - body'),
      '#format' => $config->get('external_dp_general_format') ? $config->get('external_dp_general_format') : "rich_text",
      '#description' => t('The email message sent to an external dp with optional email account.<br /><b>Available tokens:</b> [ID], [assigned_to], [reporter_name], [indication]'),
      '#default_value' => $config->get('external_dp_general_body') ? $config->get('external_dp_general_body') : ""
    ];

    $form['email']['external_dp_general_subject_recipient'] = [
      '#type' => 'textfield',
      '#title' => t('Indication has been forwarded to external department - Recipient subject line'),
      '#description' => t('The subject line for the email sent to the reporter of the indication, notifying him of the change. <br /><b>Available tokens:</b> [ID], [assigned_to], [reporter_name]'),
      '#default_value' => ($config->get('external_dp_general_subject_recipient')) ? $config->get('external_dp_general_subject_recipient') : ""
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->config('indication.external_dp_general_configuration')
      ->set('send_email_when_forwarded', $form_state->getValue('send_email_when_forwarded'))
      ->save();

    $this->config('indication.external_dp_general_configuration')
      ->set('external_dp_general_subject', $form_state->getValue('external_dp_general_subject'))
      ->save();

    $this->config('indication.external_dp_general_configuration')
      ->set('external_dp_general_body', $form_state->getValue('external_dp_general_body')['value'])
      ->save();

    $this->config('indication.external_dp_general_configuration')
      ->set('external_dp_general_format', $form_state->getValue('external_dp_general_body')['format'])
      ->save();

    $this->config('indication.external_dp_general_configuration')
      ->set('external_dp_general_subject_recipient', $form_state->getValue('external_dp_general_subject_recipient'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}

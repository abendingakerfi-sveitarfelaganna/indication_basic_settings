<?php
namespace Drupal\indication\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class EmailTextConfigurationForm extends ConfigFormBase
{

  protected function getEditableConfigNames()
  {
    return ['indication.email_text_configuration'];
  }

  public function getFormId()
  {
    return 'email_text_configuration_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('indication.email_text_configuration');

    $form['thank_you_for_submitting_subject'] = [
      '#type' => 'textfield',
      '#title' => t('Thank you for submitting - subject line'),
      '#description' => t('The subject line for the email sent to a reporter'),
      '#default_value' => ($config->get('thank_you_for_submitting_subject')) ? $config->get('thank_you_for_submitting_subject') : ""
    ];

    $form['thank_you_for_submitting_body'] = [
      '#type' => 'text_format',
      '#title' => t('Thank you for submitting - body'),
      '#format' => $config->get('thank_you_for_submitting_format') ? $config->get('thank_you_for_submitting_format') : "rich_text",
      '#description' => t('The email message sent to a reporter after they submit a new indication.'),
      '#default_value' => $config->get('thank_you_for_submitting_body') ? $config->get('thank_you_for_submitting_body') : ""
    ];

    $form['indication_has_been_assigned_subject'] = [
      '#type' => 'textfield',
      '#title' => t('Indication has been assigned - subject line'),
      '#description' => t('The subject line for the email sent to a department after the indication has been assigned.'),
      '#default_value' => ($config->get('indication_has_been_assigned_subject')) ? $config->get('indication_has_been_assigned_subject') : ""
    ];

    $form['indication_has_been_assigned_body'] = [
      '#type' => 'text_format',
      '#title' => t('Indication has been assigned - body'),
      '#format' => $config->get('indication_has_been_assigned_format') ? $config->get('indication_has_been_assigned_format') : "rich_text",
      '#description' => t('The email message sent to a department after the indication has been assigned.'),
      '#default_value' => $config->get('indication_has_been_assigned_body') ? $config->get('indication_has_been_assigned_body') : ""
    ];

    $form['indication_has_been_processed_subject'] = [
      '#type' => 'textfield',
      '#title' => t('Indication has been processed - subject line'),
      '#description' => t('The subject line for the email sent to a reporter after the indication has been processed.'),
      '#default_value' => ($config->get('indication_has_been_processed_subject')) ? $config->get('indication_has_been_processed_subject') : ""
    ];

    $form['indication_has_been_processed_body'] = [
      '#type' => 'text_format',
      '#title' => t('Indication has been processed - body'),
      '#format' => $config->get('indication_has_been_processed_format') ? $config->get('indication_has_been_processed_format') : "rich_text",
      '#description' => t('The email message sent to a reporter after the indication has been processed.'),
      '#default_value' => $config->get('indication_has_been_processed_body') ? $config->get('indication_has_been_processed_body') : ""
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('indication.email_text_configuration')
      ->set('thank_you_for_submitting_subject', $form_state->getValue('thank_you_for_submitting_subject'))
      ->save();

    $this->config('indication.email_text_configuration')
      ->set('thank_you_for_submitting_body', $form_state->getValue('thank_you_for_submitting_body')['value'])
      ->save();


    $this->config('indication.email_text_configuration')
      ->set('thank_you_for_submitting_format', $form_state->getValue('thank_you_for_submitting_body')['format'])
      ->save();

    $this->config('indication.email_text_configuration')
      ->set('indication_has_been_assigned_subject', $form_state->getValue('indication_has_been_assigned_subject'))
      ->save();

    $this->config('indication.email_text_configuration')
      ->set('indication_has_been_assigned_body', $form_state->getValue('indication_has_been_assigned_body')['value'])
      ->save();

    $this->config('indication.email_text_configuration')
      ->set('indication_has_been_assigned_format', $form_state->getValue('indication_has_been_assigned_body')['format'])
      ->save();

    $this->config('indication.email_text_configuration')
      ->set('indication_has_been_processed_subject', $form_state->getValue('indication_has_been_processed_subject'))
      ->save();

    $this->config('indication.email_text_configuration')
      ->set('indication_has_been_processed_body', $form_state->getValue('indication_has_been_processed_body')['value'])
      ->save();

    $this->config('indication.email_text_configuration')
      ->set('indication_has_been_processed_format', $form_state->getValue('indication_has_been_processed_body')['format'])
      ->save();

    parent::submitForm($form, $form_state);
  }
}

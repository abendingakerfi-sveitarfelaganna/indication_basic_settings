<?php

namespace Drupal\indication;

use DateTime;
use Drupal;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\image\Entity\ImageStyle;
use Drupal\isitaworkday\CheckDateService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\uag_html_mail\Services\UAGMail;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\taxonomy\TermInterface;

class IndicationProcessService {

  /**
   * @var CheckDateService $dateService
   */
  private $dateService;

  /**
   * @var ConfigFactoryInterface $configFactory
   */
  protected $configFactory;

  /**
   * @var EntityTypeManager $entityTypeManager
   */
  private $entityTypeManager;

  /**
   * @var string $serviceDeskName
   */
  protected $serviceDeskName;

  /**
   * @var array $serviceDeskHoursOfOperation
   */
  protected $serviceDeskHoursOfOperation;

  /**
   * @var UAGMail $mailer
   */
  protected $mailer;

  /**
   * @var ImmutableConfig $email_text_config;
   */
  protected $email_text_config;

  /**
   * @var ImmutableConfig $indication_has_been_assigned_configuration;
   */
  protected $indication_has_been_assigned_configuration;

  /**
   * @var ImmutableConfig $indication_has_been_processed_configuration;
   */
  protected $indication_has_been_processed_configuration;

  /**
   * @var ImmutableConfig $thanks_for_submitting_configuration;
   */
  protected $thanks_for_submitting_configuration;

  /**
   * @var ImmutableConfig $indication_is_insurance_matter_configuration
   */
  protected $indication_is_insurance_matter_configuration;

  /**
   * @var ImmutableConfig $indication_config
   */
  protected $indication_config;

  /**
   * IndicationProcessService constructor.
   *
   * @param ConfigFactoryInterface $config_factory
   * @param CheckDateService $date_service
   */
  public function __construct(ConfigFactoryInterface $config_factory, CheckDateService $date_service, UAGMail $mailer,
                              EntityTypeManager $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->dateService = $date_service;
    $this->mailer = $mailer;
    $this->entityTypeManager = $entity_type_manager;

    $service_desk_config = $this->configFactory->get('indication.service_desk_configuration');

    $this->serviceDeskName = $service_desk_config->get('service_desk_name');
    $this->serviceDeskHoursOfOperation['opening_hour'] = $service_desk_config->get('service_desk_opening_hour');
    $this->serviceDeskHoursOfOperation['opening_minute'] = $service_desk_config->get('service_desk_opening_minute');
    $this->serviceDeskHoursOfOperation['closing_hour'] = $service_desk_config->get('service_desk_closing_hour');
    $this->serviceDeskHoursOfOperation['closing_minute'] = $service_desk_config->get('service_desk_closing_minute');
    $this->serviceDeskHoursOfOperation['opening_string'] = str_pad($this->serviceDeskHoursOfOperation['opening_hour'] . ':' . $this->serviceDeskHoursOfOperation['opening_minute'], 5,'0',STR_PAD_LEFT);
    $this->serviceDeskHoursOfOperation['closing_string'] = str_pad($this->serviceDeskHoursOfOperation['closing_hour'] . ':' . $this->serviceDeskHoursOfOperation['closing_minute'], 5,'0',STR_PAD_LEFT);

    $this->email_text_config = $this->configFactory->get('indication.email_text_configuration');
    $this->indication_has_been_assigned_configuration = $this->configFactory->get('indication.indication_has_been_assigned_configuration');
    $this->indication_has_been_processed_configuration = $this->configFactory->get('indication.indication_has_been_processed_configuration');
    $this->thanks_for_submitting_configuration = $this->configFactory->get('indication.thanks_for_submitting_configuration');
    $this->indication_config = $this->configFactory->get('indication.indication_configuration');
    $this->indication_is_insurance_matter_configuration = $this->configFactory->get('indication.indication_is_insurance_configuration');
  }

  /**
   * New indication has just been delivered processed before saving.
   *
   * Sets indication parameters and redirects to new indication page.
   *
   *
   * @param $node
   * @param $status_array
   * @throws \Exception
   */
  public function processCreation(&$node, $status_array) {
    // Preprocess data
    $new_indication_tid = $status_array['Ný ábending'];
    $indication_tree = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term')->loadTree('indication_types', 0, null, true);
    $indication_term_array = [];
    foreach($indication_tree as $key => $indication_term) {
      $array['id'] = $indication_term->id();
      $array['array_key'] = $key;

      $indication_term_array[$indication_term->name->value] = $array;
    }
    // Set status to New Indication
    $node->set('field_indication_status_ref', $new_indication_tid);

    // Copy the indication text into the "copy" field and into the interpreted field
    $node->set('field_indication_text_copy', $node->field_indication_text->value);
    $node->set('field_interpered_indication_text', $node->field_indication_text->value);

    // Calculate response time
    $creation_date = new \DateTime(date('Y-m-dTH:i:s', $node->created->value));
    $service_desk = $indication_tree[$indication_term_array[$this->serviceDeskName]['array_key']];

    $respond_by = $this->dateService->getLastResponseDate($creation_date, $service_desk->field_indication_response_time->value, $this->serviceDeskHoursOfOperation);
    $respond_by_string = substr_replace($respond_by->format('Y-m-d H:i:s'), 'T', 10, 1);
    $node->set('field_indication_type_ref', $service_desk->id());
    $node->set('field_indication_respond_by', $respond_by_string);
    $node->set('field_reply_to_reporter_sent', FALSE);
    $node->status = 0;

    $redirect_string = $this->thanks_for_submitting_configuration->get('thank_you_for_submitting_redirect_url');
    // If redirection is configured
    if(strlen($redirect_string) > 0) {
      // If there isn't a slash in front, just add it do the string
      if(substr($redirect_string, 0, 1) != '/') {
        $redirect_string = '/' . $redirect_string;
      }
      $redirect_to_thankyou = new RedirectResponse(Url::fromUserInput($redirect_string)->toString());
      $redirect_to_thankyou->send();
    }
    else {
      $redirect_to_thankyou = new RedirectResponse(Url::fromUserInput('/')->toString());
      $redirect_to_thankyou->send();
    }
  }

  /**
   * Sends the indication to external department.
   *
   * When an indication is sent to an external department, we need to email all the recipients for that department,
   * plus any "free-form" emails that are sent with.
   *
   * We also email the one who sent the indication, informing them what happened. All messages are configured
   * in a taxonomy term under the vocabulary "Utanaðkomandi deildir og svið (external departments)".
   *
   * Finally, we set a flag saying that an email has already been sent to the external department so that we don't spam
   * them every time we are working on the indication. Also we set the status of the indication to "closed" and set the
   * time stamps for close by and closed to "now".
   *
   * @param $node
   * @param $status_array
   */
  public function processSendToExternalDp(&$node, $status_array) {
    $external_dp_configuration = $this->configFactory->get('indication.external_dp_general_configuration');
    $reporter[] = $this->getReporter($node);
    $reporter_name = (isset($reporter[0]->name)) ? $reporter[0]->name : "";
    $reporters_info = $this->getReportersInfo($node);

    // Fetch the External Department, based on the Taxonomy
    $tid = $node->field_forward_to_external_dp->target_id;
    $external_dp = ($tid)
      ? \Drupal::service('entity_type.manager')->getStorage('taxonomy_term')->load($tid)
      : null;

    if($external_dp) {
      $recipients = null;

      foreach($external_dp->field_ed_email->getValue() as $email) {
        if($email) {
          $user = new \stdClass();
          $user->name = $external_dp->getName();
          $user->mail = $email['value'];
          $recipients[] = $user;
        }
      }

      // don't send unless we have at least one email address
      if($recipients) {
        $recipients_body_text = $this->processToken(
          $external_dp->field_email_to_external_dp->getValue()[0]['value'],
          $node->id(),
          $external_dp->getName(),
          $this->getIndicationFormatted($node, FALSE, TRUE, TRUE),
          $reporter_name,
          $reporters_info
        );

        $subject = $this->processToken(
          $external_dp_configuration->get('external_dp_general_subject'),
          $node->id(),
          $external_dp->getName(),
          "",
          $reporter_name
        );
        // Sending emails to all external departments
        $params = [
          'subject' => $subject,
          'upper_body' => $recipients_body_text,
          'users' => $recipients
        ];

        $this->mailer->sendMail($params);
      }


      // Sending email to reporter
      $reporter = $this->getReporter($node);

      if($reporter) {
        if($external_dp_configuration->get('send_email_when_forwarded')) {
          $reporter_body_text = $this->processToken(
            $external_dp->field_email_to_reporter->getValue()[0]['value'],
            $node->id(),
            $external_dp->getName(),
            $this->getIndicationFormatted($node, FALSE),
            $reporter_name,
            $reporters_info
          );

          $subject = $this->processToken(
            $external_dp_configuration->get('external_dp_general_subject_recipient'),
            $node->id(),
            $external_dp->getName(),
            "",
            $reporter_name
          );
          $params = [
            'subject' => $subject,
            'upper_body' => $reporter_body_text,
            'users' => [$reporter]
          ];

          $this->mailer->sendMail($params);
          // set that the indication is closed so the reporter doesn't get two mails!
          $node->set('field_reply_to_reporter_sent', TRUE);
        }
      }
    }

    // external custom email
    $external_dp_external_email = $node->field_forward_to_external_email->getValue();
    if($external_dp_external_email) {
      $custom_dp = [];
      foreach($external_dp_external_email as $external_emails) {
        $user = new \stdClass();
        $user->name = 'no need for this';
        $user->mail = $external_emails['value'];
        $custom_dp[] = $user;
      }

      $custom_dp_body_text = $this->processToken(
        $external_dp_configuration->get('external_dp_general_body'),
        $node->id(),
        "",
        $this->getIndicationFormatted($node, FALSE),
        $reporter_name,
        $reporters_info
      );

      // need to check if we have subject since it crashes if it is not set..
      // TODO:maybe log if not set or better yet check in module if subject is set before sending = no crashing!
      $custom_dp_subject = $external_dp_configuration->get('external_dp_general_subject');
      if($custom_dp_subject && $custom_dp) {
        // Sending emails to all external departments
        $params = [
          'subject' => $custom_dp_subject,
          'upper_body' => $custom_dp_body_text,
          'users' => $custom_dp
        ];

        $this->mailer->sendMail($params);
      }

      // Email is not sent to the reporter through this channel.
    }


    // Flag that the email has been sent
    $date = new \DateTime();
    $this->closeIndication($node, $status_array);
    //    $closed_indication_tid = $status_array['Lokið'];
    $node->set('field_indication_ext_mail_sent', TRUE);
    //    $node->set('field_indication_status_ref', $closed_indication_tid);
    $node->set('field_indication_responded_at', substr_replace($date->format('Y-m-d H:i:s'), 'T', 10, 1));
    $node->set('field_indication_close_by', substr_replace($date->format('Y-m-d H:i:s'), 'T', 10, 1));
    $node->set('field_indication_closed_at', substr_replace($date->format('Y-m-d H:i:s'), 'T', 10, 1));
  }

  /**
   * Returns the status of the Service mode, being enabled or disabled.
   *
   * @return bool
   */
  public function getServiceModeStatus() : bool {
    $config = $this->configFactory->get('indication.indication_configuration');
    $config_raw_data = $config->getRawData();
    if(key_exists('service_mode_enabled', $config_raw_data)) {
      return $config->get('service_mode_enabled');
    }
    return FALSE;
  }
  /**
   * Process Indcation and send email when indication is assigned internally.
   *
   * @param $node
   * @param $status_array
   * @param $department_array
   * @throws Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @author Hilmar Kári Hallbjörnsson (drupalviking@gmail.com)
   */
  public function processAssignIndication(&$node, $status_array, $department_array) {
    $assigned_department_value = $node->get('field_indication_type_ref')->getValue();
    $assigned_department_tid = end($assigned_department_value)['target_id'];
    $assigned_department = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term')->load($assigned_department_tid);
    //check if we should be sending email to the department again.

    /** @var string $previous_department the department the indication was assigned to before this edit */
    $previous_department_value = $node->original->get('field_indication_type_ref')->getValue();
    $previous_department = end($previous_department_value)['target_id'];
    $assigned_department_has_changed = $previous_department != $assigned_department_tid;
    $send_mail = $assigned_department->field_send_mail_f_new_indication->value;
    $recipients = [];
    $reporter[] = $this->getReporter($node);
    $reporter_name = (isset($reporter[0]->name)) ? $reporter[0]->name : "";
    $reporters_info = $this->getReportersInfo($node);

    // If the Indication has not a "responded at" value, it means it was assigned by
    // service desk for the first time. Therefor we set the value + status to "Móttekin"
    if(!isset($node->field_indication_responded_at->value)) {
      $date = new \DateTime();
      $received_indication_tid = $status_array['Móttekin'];
      $close_by = $this->dateService->getLastResponseDate(new \DateTime(), $assigned_department->field_indication_response_time->value);
      $node->set('field_indication_responded_at', substr_replace($date->format('Y-m-d H:i:s'), 'T', 10, 1));
      $node->set('field_indication_close_by', substr_replace($close_by->format('Y-m-d H:i:s'), 'T', 10, 1));
      $node->set('field_indication_status_ref', $received_indication_tid);
    }

    // If it is an insurance problem and the mail has not been sent
    if($node->field_indication_insurance_prob->value == 1 && $node->field_indication_ins_mail_sent->value == 0) {
      //If not, get the emails and send the mails.
      //Let's start with the lawyer
      // TODO: make taxonomy
      $lawyer = new \stdClass();
      $lawyer->name = $this->indication_is_insurance_matter_configuration->get('indication_is_insurance_lawyers_salutation');
      $lawyer->mail = $this->indication_is_insurance_matter_configuration->get('indication_is_insurance_lawyers_email');
      $recipients[] = $lawyer;

      $email_subject_lawyer = $this->processToken(
        $this->indication_is_insurance_matter_configuration->get('indication_is_insurance_lawyers_subject'),
        $node->id(),
        $assigned_department->getName(),
        "",
        $reporter_name
      );

      if($recipients && $email_subject_lawyer) {
        $email_body_lawyer = $this->processToken(
          $this->indication_is_insurance_matter_configuration->get('indication_is_insurance_lawyers_body'),
          $node->id(),
          $assigned_department->getName(),
          $this->getIndicationFormatted($node, FALSE),
          $reporter_name,
          $reporters_info
        );

        $params = [
          'subject' => $email_subject_lawyer,
          'upper_body' => $email_body_lawyer,
          'users' => $recipients
        ];

        $this->mailer->sendMail($params);
      }

      //And then the reporter
      if(isset($node->field_indication_reporter_email->value) && $this->indication_is_insurance_matter_configuration->get('send_email_when_insurance')) {
        $email_subject_reporter = $this->processToken(
          $this->indication_is_insurance_matter_configuration->get('indication_is_insurance_subject'),
          $node->id(),
          $assigned_department->getName(),
          "",
          $reporter_name
        );
        if($email_subject_reporter) {

          $email_body_reporter = $this->processToken(
            $this->indication_is_insurance_matter_configuration->get('indication_is_insurance_body'),
            $node->id(),
            $assigned_department->getName(),
            $this->getIndicationFormatted($node, FALSE),
            $reporter_name,
            $reporters_info
          );

          $params_reporter = [
            'subject' => $email_subject_reporter,
            'upper_body' => $email_body_reporter,
            'users' => $reporter,
          ];
          $this->mailer->sendMail($params_reporter);
        }

        $node->set('field_indication_ins_mail_sent', TRUE);
      }
      // close indication if selected
      if($this->indication_is_insurance_matter_configuration->get('autoclose_when_insurance')) {
        $this->closeIndication($node, $status_array);
      }
    }

    // check if there was made any change in department assignment before getting information of whom to send to
    if($assigned_department_has_changed) {
      // If there is a group email assigned to this department, we'll get the email address
      if(isset($assigned_department->field_indication_group_email->getValue()[0]['value'])) {
        $user = new \stdClass();
        $user->name = "Safnpósthólf " . $assigned_department->getName();
        $user->mail = $assigned_department->field_indication_group_email->getValue()[0]['value'];
        $recipients[] = $user;
      }
      // If the department is supposed to get email each time an indication is assigned to them
      if($send_mail) {
        $users = $this->entityTypeManager->getStorage('user')->loadByProperties(['field_department' => $assigned_department_tid]);
        foreach($users as $user) {
          $userobj = new \stdClass();
          $userobj->name = $user->field_full_name->getValue()[0]['value'];
          $userobj->mail = $user->mail->getValue()[0]['value'];
          $recipients[] = $userobj;
        }
      }

      // If anybody is supposed to get an email, we'll send it to them
      if(sizeof($recipients) > 0) {
        /**
         * Af hverju er þetta "Original" department? Við erum ekki að fara að senda póst
         * á deildina sem var verið að taka ábendinguna af, heldur þá sem ábendingin
         * fer á.
         * Ég commenta þessar tvær línur út, þangað til annað er ákveðið og breyti:
         * $original_department_tid  =>  $assigned_department_tid
         * $original_department  =>  $assigned_department
         *
         * @author Hilmar Kári Hallbjörnsson (drupalviking)
         */
        // $original_department_tid = $node->original->field_indication_type_ref->target_id;
        // $original_department = $this->entityTypeManager->getStorage('taxonomy_term')->load($original_department_tid);

        $email_subject_line = $this->processToken(
          $this->indication_has_been_assigned_configuration->get('indication_has_been_assigned_subject'),
          $node->id(),
          $assigned_department->getName(),
          "",
          $reporter_name
        );

        $email_body = $this->processToken(
          $this->indication_has_been_assigned_configuration->get('indication_has_been_assigned_body'),
          $node->id(),
          $assigned_department->getName(),
          $this->getIndicationFormatted($node, FALSE),
          $reporter_name,
          $reporters_info
        );

        $params = [
          'subject' => $email_subject_line,
          'upper_body' => $email_body,
          'users' => $recipients,
          // get general signature here so the end user only gets user/department
          // related signatures when the indication is closed
          'signature' => $this->indication_config->get('general_signature'),
        ];

        $this->mailer->sendMail($params);
      }

      // send email to reporter regarding indication assignment
      // This function was outside the scope of this if statement, meaning it triggered
      // every time, instead of only when we changed the assignment.
      if( $this->indication_has_been_assigned_configuration->get('send_email_when_assigned') ) {
        if($reporter) {
          $body = $this->processToken(
            $this->indication_has_been_assigned_configuration->get('indication_has_been_assigned_body_reporter'),
            $node->id(),
            $assigned_department->getName(),
            $this->getIndicationFormatted($node, FALSE),
            $reporter_name,
            $reporters_info
          );

          $subject = $this->processToken(
            $this->indication_has_been_assigned_configuration->get('indication_has_been_assigned_subject_reporter'),
            $node->id(),
            $assigned_department->getName(),
            "",
            $reporter_name
          );
          if(isset($subject, $body)) {
            $params = [
              'subject' => $subject,
              'upper_body' => $body,
              'users' => $reporter,
              // get general signature here so the end user only gets user/department
              // related signatures when the indication is closed
              'signature' => $this->indication_config->get('general_signature')
            ];

            $this->mailer->sendMail($params);
          }
        }
      }

    }

    $node->status = $this->indication_config->get('publish_indications');
  }

  /**
   * House cleaning when an indication is closed.
   *
   * @param $node
   *   The node is passed by reference, therefore it doesn't have a return value.
   * @param $status_array
   * @param $department_array
   */
  public function processCloseIndication(&$node, $status_array, $department_array) {
    $reporter[] = $this->getReporter($node);
    $reporters_info = $this->getReportersInfo($node);
    $reporter_name = (isset($reporter[0]->name)) ? $reporter[0]->name : "";
    $date = new \DateTime();
    if(!isset($node->field_indication_closed_at->value)) {
      $node->set('field_indication_closed_at', substr_replace($date->format('Y-m-d H:i:s'), 'T', 10, 1));
    }
    if(!isset($node->field_indication_responded_at->value)) {
      $node->set('field_indication_responded_at', substr_replace($date->format('Y-m-d H:i:s'), 'T', 10, 1));
    }

    // Send email to reporter if that setting is enabled and he hasn't already been answered
    // via the answering mechanism.
    if(isset($node->field_indication_reporter_email->value)
      && $this->indication_has_been_processed_configuration->get('send_email_when_closed')
      && $node->field_reply_to_reporter_sent->value != TRUE) {
      $email_subject_reporter = $this->processToken(
        $this->indication_has_been_processed_configuration->get('indication_has_been_processed_subject'),
        $node->id(),
        "",
        "",
        $reporter_name
      );
      if($email_subject_reporter) {
        $email_body_reporter = $this->processToken(
          $this->indication_has_been_processed_configuration->get('indication_has_been_processed_body'),
          $node->id(),
          "",
          $this->getIndicationFormatted($node, FALSE),
          $reporter_name,
          $reporters_info
        );

        $params_reporter = [
          'subject' => $email_subject_reporter,
          'upper_body' => $email_body_reporter,
          'users' => $reporter,
        ];
        $this->mailer->sendMail($params_reporter);
      }

      $node->set('field_indication_ins_mail_sent', TRUE);
    }
  }

  /**
   * Process indication when answer is set
   *
   * Close the indication with the new reply?
   *
   *
   * @param $node
   * @param $status_array
   * @param $department_array
   * @throws \Exception
   */
  public function processAnswerIndication(&$node, $status_array, $department_array) {
    // if the indication has not been assigned to any department == new indication
    $assigned_department_tid = $node->field_indication_type_ref->target_id;
    $assigned_department = $this->entityTypeManager->getStorage('taxonomy_term')->load($assigned_department_tid);
    if(!isset($node->field_indication_close_by->value)) {
      $close_by = $this->dateService->getLastResponseDate(new \DateTime(), $assigned_department->field_indication_response_time->value);
      $node->set('field_indication_close_by', substr_replace($close_by->format('Y-m-d H:i:s'), 'T', 10, 1));
    }

    // TODO: if we have not set send email when indication is closed and the answer field is empty then change to false;
    $send_mail = true;

    // Sending email to reporter
    $reporter = $this->getReporter($node);
    $reporters_info = $this->getReportersInfo($node);

    $email_subject_line = $this->processToken(
      $this->indication_has_been_processed_configuration->get('indication_has_been_processed_subject'),
      $node->id(),
      "",
      $reporter->name
    );

    // check if we have reporter and a set subject for closed indication
    if($reporter && $email_subject_line && $send_mail) {
      $recipients[] = $reporter;
      // prepare email body!
      if(isset($node->field_indication_response->value)) {
        $body = $this->processToken(
          $node->field_indication_response->value,
          $node->id(),
          $assigned_department->getName(),
          $this->getIndicationFormatted($node, FALSE),
        );
      }
      else {
        $body = $this->processToken(
          $this->indication_has_been_processed_configuration->get('indication_has_been_processed_body'),
          $node->id(),
          $assigned_department->getName(),
          $this->getIndicationFormatted($node, FALSE),
        );
      }

      // create the parameters and get email signature
      $params = [
        'subject' => $email_subject_line,
        'upper_body' => $body,
        'users' => $recipients,
        'signature' => $this->getSignature($assigned_department),
      ];
      $this->mailer->sendMail($params);
      $node->set('field_reply_to_reporter_sent', TRUE);

      $user_id = \Drupal::currentUser()->id();
      $node->set('field_indication_answered_by_ref', $user_id);
    }

    $this->closeIndication($node, $status_array);
  }

  /**
   * @return string Name of the Service desk, as configured in the backend.
   */
  public function getServiceDeskName(): string {
    return $this->serviceDeskName;
  }


  /**
   * Sends confirmation mail in hook node insert.
   *
   * @param $node
   */
  public function sendConfirmationMail($node) {
    // Sender of the indication, if available
    $reporter = $this->getReporter($node);
    $reporters_info = $this->getReportersInfo($node);
    if($reporter) {
      if($this->thanks_for_submitting_configuration->get('send_email_when_created')) {
        $email_subject_line = $this->thanks_for_submitting_configuration->get('thank_you_for_submitting_subject');

        $recipients[] = $reporter;

        $subject_line = $this->processToken($email_subject_line, $node->id(), "", $reporter->name);

        // The email body processed with current node information
        $body = $this->processToken(
          $this->thanks_for_submitting_configuration->get('thank_you_for_submitting_body'),
          $node->id(),
          '',
          $this->getIndicationFormatted($node, FALSE),
          $reporter->name,
          $reporters_info
        );

        $params = [
          'subject' => $subject_line,
          'upper_body' => $body,
          'users' => $recipients
        ];

        $this->mailer->sendMail($params);
      }
    }
  }

  /**
   * Gets the deepest assigned department from node and returns it's signature
   * @param \Drupal\node\Entity\Node $node
   * @return string
   * @throws Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function showSignature(\Drupal\node\Entity\Node $node) {
    $tids = $node->field_indication_type_ref->getValue();
    if(is_array($tids)) {
      $assigned_department_tid = end($tids)['target_id'];
      /** @var TermInterface $assigned_department */
      $assigned_department = $this->entityTypeManager->getStorage('taxonomy_term')->load($assigned_department_tid);
      return $this->getSignature($assigned_department);
    }
  }

  /**
   * Sends email to people responsible to overdue indications. Runs from a Cronjob
   *
   * @author Hilmar Kári Hallbjörnsson (drupalviking) on 2025-02-26
   * @throws \DateMalformedIntervalStringException
   * @throws \DateMalformedStringException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function sendPassedDueEmails(): int {
    $counter = 0;
    $config = $this->configFactory->get('indication.indication_has_passed_due_date_configuration');
    $send_email = $config->get('send_email_when_indication_has_passed_due_date');
    if($send_email) {
      $recipients = null;

      $leeway = $config->get('indication_has_passed_due_date_leeway');
      $date = new \DateTime();
      if($leeway > 0) {
        $date->add(new \DateInterval('P' . $leeway . 'D'));
      }
      $date_string = $date->format('Y-m-d H:i:s');
      $date_string = str_replace(' ', 'T', $date_string);

      $config_status = $config->get('indication_has_passed_due_date_status');

      $query = \Drupal::entityQuery('node')
        ->condition('type', 'indications')
        ->condition('field_indication_close_by', $date_string, '<=')
        ->condition('field_indication_status_ref', 4)
        ->accessCheck(FALSE)
        ->execute();
      if($config_status == 'both') {
        $second_query = \Drupal::entityQuery('node')
          ->condition('type', 'indications')
          ->condition('field_indication_close_by', $date_string, '<=')
          ->condition('field_indication_status_ref', 5)
          ->accessCheck(FALSE)
          ->execute();

        $query = array_merge($query, $second_query);
      }
      if(sizeof($query) > 0) {
        $nodes = Node::loadMultiple($query);
        foreach ($nodes as $node) {
          if(isset($node->field_indication_p_due_mail_sent->getValue()[0]['value'])) {
            $has_been_sent = $node->field_indication_p_due_mail_sent->getValue()[0]['value'];
            if($has_been_sent) {
              continue;
            }
          }
          $assigned_department_value = $node->get('field_indication_type_ref')->getValue();
          $assigned_department_tid = end($assigned_department_value)['target_id'];
          $assigned_department = Term::load($assigned_department_tid);
          if(isset($assigned_department->field_indication_group_email->getValue()[0]['value'])) {
            $user = new \stdClass();
            $user->name = "Safnpósthólf " . $assigned_department->getName();
            $user->mail = $assigned_department->field_indication_group_email->getValue()[0]['value'];
            $recipients[] = $user;
          }
          $users = $this->entityTypeManager->getStorage('user')->loadByProperties(['field_department' => $assigned_department_tid]);
          foreach($users as $user) {
            $userobj = new \stdClass();
            $userobj->name = $user->field_full_name->getValue()[0]['value'];
            $userobj->mail = $user->mail->getValue()[0]['value'];
            $recipients[] = $userobj;
          }

          // If anybody is supposed to get an email, we'll send it to them
          if(sizeof($recipients) > 0) {
            $email_subject_line = $this->processToken(
              $config->get('indication_has_passed_due_date_subject'),
              $node->id(),
              $assigned_department->getName()
            );

            $close_by = new DateTime($node->get('field_indication_close_by')->getValue()[0]['value']);
            $assigned_date = new DateTime($node->get('field_indication_responded_at')->getValue()[0]['value']);
            $close_by_string = $close_by->format('d. M, Y');
            $assigned_date_string = $assigned_date->format('d. M, Y');
            $email_body = $this->processToken(
              $config->get('indication_has_passed_due_date_body'),
              $node->id(),
              $assigned_department->getName(),
              $this->getIndicationFormatted($node, FALSE),
              "",
              "",
              $assigned_date_string,
              $close_by_string
            );

            $params = [
              'subject' => $email_subject_line,
              'upper_body' => $email_body,
              'users' => $recipients,
              // get general signature here so the end user only gets user/department
              // related signatures when the indication is closed
              'signature' => $this->indication_config->get('general_signature'),
            ];

            // $this->mailer->sendMail($params);
            $node->set('field_indication_p_due_mail_sent', TRUE);
            $node->save();
            $counter++;
          }
        }
      }
    }

    return $counter;
  }

  /**
   * Closes the indication
   */
  private function closeIndication(&$node, $status_array) {
    $closed_indication_tid = $status_array['Lokið'];
    $node->set('field_indication_status_ref', $closed_indication_tid);
  }

  /**
   * Returns wrapped indication as a string in wrapper. If checked, the
   * interpreted version is used.
   *
   * @param $node
   * @param bool $interpreted
   * @param bool $images
   * @param bool $location
   * @return string a wrapped indication
   * @author Hólmfríður Magnea Hákonardóttir
   * @coauthor Hilmar Kári Hallbjörnsson (drupalviking@gmail.com)
   */
  private function getIndicationFormatted($node, bool $interpreted = FALSE, bool $images = TRUE, bool $location = TRUE) : string {
    $text = (sizeof($node->field_indication_text->getValue()) > 0)
      ? $node->field_indication_text->getValue()[0]['value']
      : '';

    $interpreted_text = (sizeof($node->field_indication_text->getValue()) > 0)
      ? $node->field_indication_text->getValue()[0]['value']
      : '';

    $email_text = ($interpreted) ? $interpreted_text : $text;

    $indication = '<div class="indication"><div class="indication-text">
            <h3 class="indication__title">' . $node->label() . '</h3>
            <p class="indication__body">'.$email_text.'</p>
            </div>';

    $node_images = $node->field_image->getValue();
    if($images && sizeof($node_images) > 0) {
      $indication .= '<div class="images">';
      $indication .= '<h3>' . t('Images') . '</h3>';
      $indication .= '<ul>';
      foreach($node_images as $image) {
        $img = File::load($image['target_id']);
        $style = ImageStyle::load('original_small');
        $image_style_uri = $style->buildUri($img->getFileUri());
        $uri = str_replace('public://', \Drupal::request()->getSchemeAndHttpHost() . '/sites/default/files/', $image_style_uri);
        $large_uri = str_replace('public://', \Drupal::request()->getSchemeAndHttpHost() . '/sites/default/files/', $img->getFileUri());
        //$indication .= '<li class="image"><a href="' . $large_uri . '"><img src="' . $uri . '" /></a></li>';
        $indication .= '<li class="image_link"><a href="' . $large_uri . '">' . $img->getFilename() . '</a></li>';
      }
      $indication .= '</ul>';
      $indication .= '</div>';
    }

    $location_data = $node->field_location_data->getValue();
    if($location) {
      $indication .= '<div class="map_embed">';
      //$indication .= '<iframe width="100%" height="400" src="https://maps.google.com/maps?q=' . $location_data[0]['lat'] . ',' . $location_data[0]['lng'] .'&hl=en-US&amp;z=17&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe>';
      $indication .= '<a href="https://maps.google.com/maps?q=loc:' . $location_data[0]['lat'] . ',' . $location_data[0]['lng'] . '">' . t('Google Maps Location') . '</a>';
      $indication .= '</div>';
    }

    $indication .= '</div>';
    return $indication;
  }


  /**
   * Finds the reporter email.
   * If available returns stdClass obj with name and email else false
   *
   * @param $node
   * @return \stdClass|false
   */
  private function getReporter($node) : \stdClass | false{
    $senders_email = (sizeof($node->field_indication_reporter_email->getValue()) > 0)
      ? $node->field_indication_reporter_email->value
      : null;
    if(isset($senders_email)) {
      $senders_name = (sizeof($node->field_indication_reporter_name->getValue()) > 0)
        ? $node->field_indication_reporter_name->getValue()[0]['value']
        : $this->indication_config->get('general_user_name');
      $user = new \stdClass();
      $user->name = $senders_name;
      $user->mail = $senders_email;
      return $user;
    }
    return false;
  }

  /**
   * Extracts all the user data from the node and presents it as a string,
   * to be used with the tokenizer.
   *
   * @param $node
   *
   * @return string
   */
  public function getReportersInfo($node) : string {
    $senders_name = (sizeof($node->field_indication_reporter_name->getValue()) > 0)
      ? $node->field_indication_reporter_name->getValue()[0]['value']
      : $this->indication_config->get('general_user_name');

    $senders_email = (sizeof($node->field_indication_reporter_email->getValue()) > 0)
      ? $node->field_indication_reporter_email->value
      : null;

    $senders_telephone = (sizeof($node->field_indication_reporter_phone->getValue()) > 0)
      ? $node->field_indication_reporter_phone->value
      : null;

    $reporters_info = "<div class=\"reporters-info\">";
    $reporters_info .= "<p>Sendandi: " . $senders_name . "</p>";
    $reporters_info .= (isset($senders_email)) ? "<p>Tölvupóstur: " . $senders_email . "</p>": "";
    $reporters_info .= (isset($senders_telephone)) ? "<p>Sími: " . $senders_telephone . "</p>" : "";
    $reporters_info .= "</div>";

    return $reporters_info;
  }

  /**
   * Gets email signature
   *
   * @return string
   * @author Holmfridur
   */
  private function getSignature(Drupal\taxonomy\TermInterface|null $assigned_department) {
    $signature = '';
    $accountProxy = \Drupal::currentUser();
    /** @var Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')->load($accountProxy->id());
    $use_type_signature = $user->get('field_use_type_signature')->getValue() ? $user->get('field_use_type_signature')->getValue()[0]['value'] : false;

    if (!$use_type_signature) {
      $user_value = $user->get('field_signature')->getValue();
      $signature = sizeof($user_value) > 0 ? $user_value[0]['value'] : '';
    }
    if(!$signature || strlen($signature) == 0){
      if(!is_null($assigned_department)) {
        $dep_value = $assigned_department->get('field_signature')->getValue();
        $signature = $dep_value ? $dep_value[0]['value'] : '';
      }
      else {
        $signature = $this->indication_config->get('general_signature');
      }
    }
    if(!$signature || strlen($signature) == 0){
      $signature = $this->indication_config->get('general_signature');
    }

    return $signature;
  }

  /**
   * Replaces tokens within a string
   *
   * @param string $string_to_process
   * @param string $nodeId
   * @param string $assignedTo
   * @param string $indication
   * @param string $reporter
   * @param string $reporters_info
   * @param string $close_by
   *
   * @return string
   * @author Hólmfríður Magnea Hákonardóttir (holmfridurmh@gmail.com)
   */
  private function processToken(string $string_to_process, string $nodeId = '',
                                string $assignedTo = '', string $indication = '',
                                string $reporter = '', string $reporters_info = '',
                                string $assigned_date = '', string $close_by = ''): string {
    $tokens =  [
      '[ID]', '[assigned_to]', '[indication]', '[reporter_name]', '[reporters_info]',
      '[assigned_date]', '[close_by]'
    ];
    $value = [
      $nodeId, $assignedTo, $indication, $reporter, $reporters_info, $assigned_date,
      $close_by
    ];
    return str_replace($tokens, $value, $string_to_process);
  }
}

<?php

namespace Drupal\responsible_party_display\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultReasonInterface;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\taxonomy\Entity\Term;
use JetBrains\PhpStorm\Pure;

/**
 * Provides a block with revision information for indications
 *
 * @Block(
 *   id = "responsible_party_display",
 *   admin_label = @Translation("Responsible Party Display Block"),
 * )
 */
class ResponsiblePartyDisplay extends BlockBase {

  public function build()
  {
    $responsible_parties = $this->assembleDisplay();
    $header = [
      'display_name' => t('Name'),
    ];

    $build['responsible_parties'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $responsible_parties,
      '#empty' => t('No Responsible Parties found')
    ];

    return [
      '#type' => 'markup',
      '#markup' => \Drupal::service('renderer')->render($build),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account): AccessResultReasonInterface|AccessResult
  {
    return AccessResult::allowedIfHasPermission($account, 'view responsible party display');
  }

  /**
   * {@inheritdoc}
   */
  #[Pure] public function blockForm($form, FormStateInterface $form_state): array {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['responsible_party_display_settings'] = $form_state->getValue('responsible_party_display_settings');
  }

  private function assembleDisplay() : array {
    $array = [];
    $responsible_users_array = [];
    $node = \Drupal::routeMatch()->getParameter('node');
    $divisions = $node->get('field_indication_type_ref')->getValue();

    $service_desk_config = \Drupal::config('indication.service_desk_configuration');
    $service_desk_name = $service_desk_config->get('service_desk_name');
    $service_desk_tid = \Drupal::entityQuery('taxonomy_term')
      ->condition('name', $service_desk_name)
      ->accessCheck(FALSE)
      ->execute();
    $service_desk_tid = array_values($service_desk_tid)[0];
    if(sizeof($divisions) == 1 && $divisions[0]['target_id'] == $service_desk_tid) {
      return [['display_name' => $service_desk_name]];
    }
    else {
      foreach($divisions as $division) {
        $division_id = $division['target_id'];
        $responsible_users = \Drupal::entityQuery('user')
          ->condition('field_department', $division_id)
          ->accessCheck(FALSE)
          ->execute();

        foreach($responsible_users as $key => $value) {
          $responsible_users_array[$key] = $value;
        }
      }

      if(sizeof($responsible_users_array) > 0) {
        $users = \Drupal\user\Entity\User::loadMultiple($responsible_users_array);

        foreach($users as $responsible_user) {
          $username = $responsible_user->getDisplayName();
          $full_name = strlen($responsible_user->get('field_full_name')->value) > 0
            ? $responsible_user->get('field_full_name')->value
            : 'Fullt nafn ekki skráð';
          $array[] = [
            'display_name' => $full_name . ' (' . $username . ' )',
          ];
        }

        return $array;
      }
      else {
        return [];
      }
    }

  }

  protected function getAllDepartments() : array {
    $return_array = [];
    $department_tids = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', 'indication_types')
      ->accessCheck(FALSE)
      ->execute();
    $departments = Term::loadMultiple($department_tids);
    foreach($departments as $tid => $department) {
      $return_array[$tid] = $department->getName();
    }
    return $return_array;
  }
}
